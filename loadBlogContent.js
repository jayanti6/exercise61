function LoadBlogContent(domElements){
  this.headings = domElements.headings;
}

LoadBlogContent.prototype.init = function(){
  this.createDivInsideHeadings();
  this.bindClickToHeadings();
}

LoadBlogContent.prototype.createDivInsideHeadings = function(){
  this.headings.each(function(){
    var targetDiv = $("<div></div>");
    $(this).append(targetDiv).data("targetDiv", targetDiv);
  });
}

LoadBlogContent.prototype.bindClickToHeadings = function(){
  this.headings.on("click", function(event){
    var $this = $(this);
    event.preventDefault();
    var divToFetch = $this.find("a").attr("href").split("#")[1];
    var targetDiv = $this.data("targetDiv");
    targetDiv.load("data/blog.html #" + divToFetch);
  })
}

$(document).ready(function(){
  var domElements = {
    headings: $("#blog").find("h3")
  }

  var loadBlogContent = new LoadBlogContent(domElements);
  loadBlogContent.init();
});
